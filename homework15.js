// Напишите как вы понимаете рекурсию. Для чего она используется на практике?

// Рекурсия – это приём программирования, когда задача может быть естественно разделена на несколько  более простых задач.
// Рекурсивный способ: упрощение задачи и вызов функцией самой себя

let userNumber = prompt("введите число");
while (userNumber === "" || userNumber === null || Number.isNaN(+userNumber)) {
  userNumber = prompt("введите число");
}
function pow(userNumber) {
  if (userNumber == 1) {
    return userNumber;
  } else {
    return userNumber * pow(userNumber - 1);
  }
}

alert(pow(userNumber));
